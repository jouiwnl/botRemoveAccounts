import mongoose from 'mongoose';
import configs from '../../configs/configs.js';
import logger from '../logger/index.js';

const url = configs.MONGO_URL;

try {
  // Connect to the MongoDB cluster
  mongoose.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => logger.info(' Mongoose is connected'),
  );
} catch (e) {
  logger.error('could not connect');
}

mongoose.Promise = global.Promise;

export default mongoose;