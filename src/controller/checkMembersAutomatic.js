import Term from '../model/Term.js';
import _ from 'lodash';
import logger from '../logger/index.js';

const checkMembersAutomatic = async (servidor) => {

  if (_.isNull(servidor)) {
    return;
  }

  const terms = await Term.find({ guildId: servidor.id });

  servidor.members.fetch().then(membros => {
    membros.map(membro => {
      if (membro.user.username != undefined) {
        terms.map(word => {
          if (membro.user.username.split(' ').join('').toLowerCase().match(word.word.toLowerCase())) {
            membro.ban({ reason: 'Você foi banido do servidor' })
              .then(res => {
                logger.info(`${membro.user.username} banido do servidor!`);
              })
              .catch(err => { 
                logger.error(`Erro ao banir ${membro.user.username}!`);
              }); 
          }
        });
      }
    });
  });
};

export default checkMembersAutomatic;
