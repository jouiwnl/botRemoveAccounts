import Term from '../model/Term.js';
import logger from '../logger/index.js';

const checkMembers = async (membro) => {

  const terms = await Term.find({ guildId: membro.guild.id });
    
  terms.map(word => {
    if (membro.user.username.split(' ').join('').toLowerCase().match(word.word.toLowerCase())) {
      membro.ban({ reason: 'Você foi banido do servidor' })
        .then(res => {
          logger.info(`${membro.user.username} banido do servidor!`);
        })
        .catch(err => { 
          logger.error(`Erro ao banir ${membro.user.username}!`);
        });  
    }
  });
};

export default checkMembers;
