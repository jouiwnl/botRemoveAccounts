import express from 'express';
import logger from '../logger/index.js';
const server = express();

server.all('/', (req, res) => {
  res.send('Bot is running');
});  
  
const openServer = () => {
  server.listen(process.env.PORT || 3355, () => {
    logger.info('Server is running');
  });
};

export default openServer;
